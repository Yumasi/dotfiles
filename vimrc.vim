syntax on
filetype plugin indent on

set tabstop=8
set shiftwidth=8
set number

set listchars=eol:¬,tab:>-,trail:~,extends:>,precedes:<
set list

execute pathogen#infect()

set background=dark
colorscheme solarized

let g:EasyMotion_do_mapping = 0

nmap <Leader>s <Plug>(easymotion-overwin-f)

let g:EasyMotion_smartcase = 1

map <Space> <Leader>

map <Leader>j <Plug>(easymotion-j)
map <Leader>k <Plug>(easymotion-k)

set cc=80
