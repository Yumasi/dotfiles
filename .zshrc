# Setting prompt
PROMPT="%n@%m %d %# "

# Adding Cask to PATH
export PATH="$HOME/.cask/bin:$PATH"

# Pretty ls
eval `dircolors ~/.ls_colors`

alias e="emacsclient -t --alternate-editor="""
alias emacs="emacsclient -t --alternate-editor="""
alias ls="ls --color"
alias reboot="sudo reboot"
alias poweroff="sudo poweroff"
alias halt="sudo halt"
