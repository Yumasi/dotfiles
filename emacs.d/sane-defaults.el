;;; sane-defaults.el -- Must-have settings for base emacs

(defun sane-defaults/disable-menu-bar ()
  "Remove the useless menu bar."
  (menu-bar-mode -1))

(defun sane-defaults/enable-trailing-whitespace ()
  "Show me trailing whitespaces."
  (setq-default show-trailing-whitespace t))

(defun sane-defaults/disable-backup-files ()
  "Disable excessive backup files creation."
  (setq make-backup-files nil))

(defun sane-defaults/disable-splash-screen ()
  "Don't show the useless splash screen on startup."
  (setq inhibit-splash-screen t))

(defun sane-defaults/follow-symlinks ()
  "Prevents emacs from asking if it must follow a symlink,
   and follows it automatically instead."
  (setq vc-follow-symlinks t))

(defun sane-defaults/delete-trailing-whitespace ()
  "Removes all trailing whitespaces from file before saving."
  (add-hook 'before-save-hook 'delete-trailing-whitespace))

(defun sane-defaults/always-highlight ()
  "Turn on syntax highlighting if possible."
  (global-font-lock-mode t))

(defun sane-defaults/show-matching-parens ()
  "Show matching pairs of parentheses."
  (show-paren-mode t)
  (setq show-pare-delay 0.0))

(defun sane-defaults/set-default-line-length ()
  "Set default line length."
  (setq-default fill-column 80))

(defun sane-defaults/sanitize-editor ()
  "Load all sane settings."
  (sane-defaults/follow-symlinks)
  (sane-defaults/disable-menu-bar)
  (sane-defaults/enable-trailing-whitespace)
  (sane-defaults/disable-backup-files)
  (sane-defaults/disable-splash-screen)
  (sane-defaults/delete-trailing-whitespace)
  (sane-defaults/always-highlight)
  (sane-defaults/show-matching-parens)
  (sane-defaults/set-default-line-length))
