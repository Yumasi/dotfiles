syntax on

filetype plugin indent on
set tabstop=4
set shiftwidth=4

set listchars=eol:¬,tab:>-,trail:~,extends:>,precedes:<

execute pathogen#infect()

let g:EasyMotion_do_mapping = 0

nmap <Leader>s <Plug>(easymotion-overwin-f)

let g:EasyMotion_smartcase = 1

map <Space> <Leader>

map <Leader>j <Plug>(easymotion-j)
map <Leader>k <Plug>(easymotion-k)

set cc=80
