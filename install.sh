#!/bin/bash
mkdir -p ~/.vim/autoload ~/.vim/bundle && \
curl -LSso ~/.vim/autoload/pathogen.vim https://tpo.pe/pathogen.vim

cp -f ~/dotfiles/Xresources ~/.Xresources
ln -s -f ~/dotfiles/.zshrc ~/.zshrc
ln -s -f ~/dotfiles/vimrc.vim ~/.vimrc
ln -s -f ~/dotfiles/config ~/.config/i3/config
ln -s -f ~/dotfiles/emacs.d/init.el ~/.emacs.d/init.el
ln -s -f ~/dotfiles/emacs.d/config.org ~/.emacs.d/config.org
ln -s -f ~/dotfiles/emacs.d/sane-defaults.el ~/.emacs.d/sane-defaults.el
ln -s -f ~/dotfiles/ls_colors ~/.ls_colors
git clone https://github.com/cask/cask.git ~/.cask

ln -s -f ~/dotfiles/emacs.d/Cask ~/.emacs.d/Cask
